# Node.js task - sollution

Hey there!

This is the first task in recruitment process for the position of Node.js Developer at Netguru. Read the instructions carefully and send us back the link with solution.

### Specification

We’d like you to build simple REST API for us - a basic movie database interacting with external API. Here’s full specification of endpoints that we’d like it to have:

* `POST /movies`:
  - [x] Request body should contain only movie title, and its presence should be validated.
  - [x] Based on passed title, other movie details should be fetched from http://www.omdbapi.com/ (or other similar, public movie database) - and saved to application database.
  - [x] Request response should include full movie object, along with all data fetched from external API.
* `GET /movies`:
  - [x] Should fetch list of all movies already present in application database.

  - [x] Additional filtering, sorting is fully optional - but some implementation is a bonus.

    Filtering is implemented. You can filter based by every property existed in Object got from omdbapi. 

    #### Examples:

    - by movie title

    ```http
    GET /movies?title=Sherlock 
    ```

    - by movie genre

    ```http
    GET /movies?genre=action
    ```

    
* `POST /comments`:
  - [x] Request body should contain ID of movie already present in database, and comment text body.
  - [x] Comment should be saved to application database and returned in request response.
* `GET /comments`:
  - [x] Should fetch list of all comments present in application database.

  - [x] Should allow filtering comments by associated movie, by passing its ID.

    #### Example:

    ```http
    GET /comments/5b296418dc716e769359bc43
    ```

## Installation 

Application is running with the latest version of NodeJs and npm.

To working properly application needs to set some environment variables:

```shell
export MOVIE_APIKEY=yourKey
export MOVIE_API=http://www.omdbapi.com/
export MONGODB_URI=yourMongoUri
export PORT=portToYourAPP
```

- npm installation scripts

```sh
npm install # to install depenencies
npm run build # to build app
npm run start # to start app
npm run test # to see app tests
```

API will be available on address of your server, and PORT defined in env.