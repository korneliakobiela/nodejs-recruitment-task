import axios from 'axios';
import Logger from '../../utils/Logger';
import { createMoviesQuery } from '../../utils/QueryUtils';
import Movie from '../models/MovieModel';

const controller = {

	errorLogger: new Logger( 'error', 'error.log' ),
	info: new Logger( 'info' ),

	/**
	 * Fetching movie details from external database
	 *
	 * @param {string} title Serialized movie title
	 * @return {Promise} Response from API
	 */
	fetchMovieDetails( title ) {
		const apikey = process.env.MOVIE_APIKEY;
		const movieApi = process.env.MOVIE_API;
		return axios.get( `${movieApi}?t=${title}&apikey=${apikey}` );
	},

	/**
	 * addMovie - Adding movie to the db
	 *
	 * @param  {Object} req request object
	 * @param  {Object} res Response object
	 * @return {Promise}     description
	 */
	addMovie( req, res ) {
		let title = req.body.title;
		controller.fetchMovieDetails( title ).
			then( ( details ) => {
				title = title.toLowerCase().trim();
				return Movie.findOneAndUpdate( { title },
					{ title, details: details.data },
					{ upsert: true, new: true } );
			} ).
			then( ( movie ) => {
				res.send( movie );
			} ).
			catch( ( err ) => {
				res.status( 422 ).send( err );
			} );
	},

	/**
	 * fetchMovies - Get movies from database
	 *
	 * @param  {Object} req request object
	 * @param  {Object} res Response object
	 * @return {Promise}     description
	 */
	fetchMovies( req, res ) {
		const query = createMoviesQuery( req.query );
		Movie.find(  query ).populate( 'comment' ).
			then( ( movies ) => {
				res.send( movies );
			} ).catch( ( err ) => {
				res.send( err );
			} );
	}
};

export default controller;
