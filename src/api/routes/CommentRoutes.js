import express from 'express';
import controller from '../controllers/CommentsController';

const router =  express.Router(); // eslint-disable-line new-cap
router.post( '/', controller.addComment );
router.get( '/', controller.getComments );
router.get( '/:movie', controller.getCommentsByMovieId );

export default router;
