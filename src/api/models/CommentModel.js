import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const commentSchema = new Schema( {
	movie: {
		type: Schema.Types.ObjectId,
		ref: 'Movies'
	},
	comment: {
		type: String,
		required: true,
		maxlength: 1024
	}
} );

export default mongoose.model( 'Comments', commentSchema );
