const expect = require( 'chai' ).expect;
const Logger = require( '../../src/utils/Logger' );
const fs = require( 'fs' );

describe( 'Logger', () => {

	let consoleLogger, fileLogger;

	beforeEach( () => {
		consoleLogger = new Logger.default( 'info' );
		fileLogger = new Logger.default( 'info', 'logfile.log' );
	} );

	afterEach( ( done ) => {
		fs.unlink( 'logfile.log', () => {
			done();
		} );
	} );

	it( 'should create logger instance', () => {
		expect( consoleLogger.logger ).not.to.be.an( 'undefined' );
	} );

	describe( '_isFileTransport', () => {

		it( 'is false when filename is not given', () => {
			expect( consoleLogger._isFileTransport() ).to.be.false;
		} );

		it( 'is true if filename is given', () => {
			expect( fileLogger._isFileTransport() ).to.be.true;
		} );

	} );

	describe( '_createTransport', () => {

		it( 'should return array', () => {
			expect( consoleLogger._createTransport() ).to.be.an( 'array' );
		} );

		it( 'return one-piece array when filename is not given', () => {
			expect( consoleLogger._createTransport().length ).to.equal( 1 );
		} );

		it( 'return two-piece array when filename is given', () => {
			expect( fileLogger._createTransport().length ).to.equal( 2 );
		} );

	} );

	describe( 'log', () => {

		it( 'log to file when filaname is given', ( done ) => {
			const message = 'This is logging message';
			fileLogger.log( message );
			fs.readFile( 'logfile.log', 'utf8', ( err, data ) => {
				expect( data.trim() ).to.equal( `info: ${message}` );
				done();
			} );
		} );

	} );

} );
